<?php

class Page{

	private $Tpl;

	public $options = array(
		"header"=>true,
		"footer"=>true,
		"data"=>array(
			"head_title"=>"Caminhos de Diamantina",
			"meta_description"=>"",
			"meta_author"=>"Anthony Rafael Ribeiro/João Rangel"
		)
	);

	public function __construct($options = array()){

		$rootdir = PATH;

		rainTpl::configure("base_url", $rootdir);
		rainTpl::configure("tpl_dir", $rootdir."/res/tpl/");
		rainTpl::configure("cache_dir", $rootdir."/res/tpl/tmp/");
		rainTpl::configure("path_replace", false);

		$options = array_merge($this->options, $options);//Funde dois arrays

		$options['data']['path'] = SITE_PATH;

		$tpl = $this->getTpl();
		$this->options = $options;

		if(gettype($this->options['data']) == 'array'){

			foreach($this->options['data'] as $key=>$val){
				$tpl->assign($key, $val);
			}

		}

		if ($this->options['header'] === true) $tpl->draw("header", false);

	}

	public function __destruct(){

		$tpl = $this->getTpl();

		if(gettype($this->options['data']) == 'array'){

			foreach($this->options['data'] as $key=>$val){
				$tpl->assign($key, $val);
			}

		}

		if($this->options['footer'] === true) $tpl->draw("footer", false);

	}	

	public function setTpl($tplname, $data = array(), $returnHTML = false){

		$tpl = $this->getTpl();

		if(gettype($data) == 'array'){
			foreach($data as $key=>$val){
				$tpl->assign($key, $val);
			}
		}

		return $tpl->draw($tplname, $returnHTML);

	}

	public function getTpl(){

		return ($this->Tpl)?$this->Tpl:$this->Tpl = new RainTpl;

	}	

}

?>