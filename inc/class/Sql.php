<?php

class Sql{

	public $conn;

	const MYSQL = 1;
	const SQLSERVER = 2;

	private $db_type = DB_TYPE;
	private $host = HOST;
	private $user = USER;
	private $password = PASSWORD;
	private $db = DB;

	private $utf8 = true;

	public function conecta($config = array()){

		if(count($config)){

			$this->host = $config['host'];
			$this->user = $config['user'];
			$this->password = $config['password'];
			$this->db = $config['db'];

		}

		if($this->db_type === Sql::MYSQL){
			return $this->conectaMySql();
		}

	}

	private function conectaMySql(){

		$this->conn = @mysqli_connect($this->host, $this->user, $this->password);

		if(!$this->conn){
			throw new Exception("Não foi possível conectar com o banco de dados");
		}

		if(!@mysqli_select_db($this->conn, $this->db)){
			throw new Exception("O banco de dados ".$this->db." não foi encontrado.");
		}

		return $this->conn;

	}

	public function getType(){

		return (int)$this->type;

	}

	public function __construct(){

		if($this->db !== "database_name_here"){
			return $this->conecta();
		}

	}

	public function desconecta(){

		if($this->db_type === Sql::MYSQL){
			return mysqli_close($this->conn);
		}

	}

	public function query($query, $params = array(), $multi = false){

		$this->conecta();

		if(count($params)){

			$query = str_replace('?', '{?}', $query);

			$query = $this->setParamsToQuery($query, $this->trataParams($params));

		}

		if($_SERVER['HTTP_HOST'] === "localhost" && isset($_GET['query_debug'])){
			pre($query);
		}

		$resource = false;

		if(!$query) throw new Exception("Informe a query");

		try{

			if($this->db_type === Sql::MYSQL){

				if($multi === false){
					$resource = mysqli_query($this->conn, $query);
				}else{
					$query = str_replace(';;', ';', $query);
					$resource = mysqli_multi_query($this->conn, $query.';');
				}

			}

		}catch(Exception $e){

			var_dump($e, debug_backtrace());

		}

		if(!$resource){

			if($this->db_type === Sql::MYSQL){
				throw new Exception(mysqli_error($this->conn));		
			}

		}

		return $resource;
	}

	private function setParamsToQuery($query, $params = array()){

		if(strpos($query, '{?}')>-1 && count($params) > 0){

			$first = array_shift($params);
			$query = preg_replace('/\{\?\}/', $first, $query, 1);

			return $this->setParamsToQuery($query, $params);

		}else{

			return $query;

		}

	}

	public function trataParams($params = array()){

		$params_new = array();

		foreach ($params as $value) {

			switch(gettype($value)){
				case 'string':
				$value = ($this->utf8 === true)?utf8_decode($value):$value;
				array_push($params_new, "'".$value."'");
				break;
				case 'integer':
				case 'float':
				case 'double':
				array_push($params_new, $value);
				break;
				case 'bool':
				case 'boolean':
				array_push($params_new, (($value)?1:0));
				break;
				case 'null':
				array_push($params_new, 'NULL');
				break;
				default:
				array_push($params_new, "''");
				break;
			}

		}

		return $params_new;

	}

	public function select($query, $params = array()){
		
		return $this->arrays($query, true, $params);
			
	}

	private function getFieldsFromResouce($resource){

		$fields = array();

		switch($this->db_type){

			case SQL::MYSQL:
			if(gettype($resource) === 'object'){
				$finfo = $resource->fetch_fields();
			    foreach($finfo as $val){
					array_push($fields, array(
						"field"=>$val->name,
						"type"=>strtoupper($val->type),
						"max_length"=>$val->max_length
					));
				}
			}
			break;

			case SQL::SQLSERVER:
			foreach(sqlsrv_field_metadata($resource) as $field){
				array_push($fields, array("field"=>$field['Name'], "type"=>strtoupper($field['Type']), "max_length"=>$field['Size']));
			}
			break;

		}

		return $fields;

	}

	public function getArrayRows($resource){

		$fields = $this->getFieldsFromResouce($resource);

		$data = array();
		
		switch($this->db_type){

			case SQL::SQLSERVER:

				while($a1 = sqlsrv_fetch_array($resource)){
		            $record = array();
		            foreach($fields as $f) {
						
		                switch ((int)$f['type']) {
							case 4:
								$record[$f['field']] = (int)($a1[$f['field']]);
								break;
							case -6:
								$record[$f['field']] = (int)($a1[$f['field']]);
								break;
							case 3:
								$record[$f['field']] = (float)($a1[$f['field']]);
								break;
							case 16:
								$record[$f['field']] = (int)($a1[$f['field']]);
								break;
							case 20:
								$record[$f['field']] = (float)($a1[$f['field']]);
								break;
							case 11:
								$record[$f['field']] = (bool)($a1[$f['field']]);
								break;
							case -7:
								$record[$f['field']] = (bool)($a1[$f['field']]);
								break;
							case 6:
								$record[$f['field']] = (float)number_format($a1[$f['field']], 2, '.', '');
								break;
							case 14:
								$record[$f['field']] = (float)number_format($a1[$f['field']], 2, '.', '');
								break;
							case -154:
								if($datetime){
									$record[$f['field']] = ($a1[$f['field']])?$a1[$f['field']]:NULL;
								}else{
									$record[$f['field']] = ($a1[$f['field']])?$a1[$f['field']]->format('H:i:s'):NULL;
								}
							break;
							case 7:
								if($datetime){
									if($datasql){
										$record[$f['field']] = ($a1[$f['field']])?$a1[$f['field']]->format('Y-m-d H:i'):NULL;
									}else{
										$record[$f['field']] = ($a1[$f['field']])?$a1[$f['field']]:NULL;
									}
								}else{
									$record[$f['field']] = ($a1[$f['field']])?$a1[$f['field']]->format('U'):NULL;
								}
								$record["des".$f['field']] = date("d/m/Y H:i", $record[$f['field']]);
							break;	
							case 93:
								if($datetime){
									if($datasql){
										$record[$f['field']] = ($a1[$f['field']])?$a1[$f['field']]->format('Y-m-d H:i'):NULL;
									}else{
										$record[$f['field']] = ($a1[$f['field']])?$a1[$f['field']]:NULL;
									}
								}else{
									$record[$f['field']] = ($a1[$f['field']])?$a1[$f['field']]->format('U'):NULL;
									$record["des".$f['field']] = date("d/m/Y", $record[$f['field']]);
								}
							break;
							case 12:
								$record[$f['field']] = trim(utf8_encode(trim($a1[$f['field']])));
								break;
							case 91:
								$record[$f['field']] = strtotime($a1[$f['field']]->format('Y-m-d H:i:s'));
								break;
							default:
								$record[$f['field']] = trim(utf8_encode(trim($a1[$f['field']])));
								break;
							}
		            }
		            if(is_array($record)) array_push($data, $record);
		        }

			break;

			case SQL::MYSQL:

				while(gettype($resource) === 'object' && $a1 = $resource->fetch_array()){
					
					$record = array();
					
					foreach($fields as $f){

						switch($f['type']){
							case 'INT':
							case 3:
							case 8:
							$record[$f['field']] = (int)$a1[$f['field']];
							break;

							case 'BIT':
							case 16:
							$record[$f['field']] = (bool)$a1[$f['field']];
							break;

							case 'DATETIME':
							case 7:
							if ($a1[$f['field']] === NULL) {
								$record[$f['field']] = NULL;
								$record['des'.$f['field']] = NULL;
							} else {
								$record[$f['field']] = strtotime($a1[$f['field']]);
								$record['des'.$f['field']] = date('d/m/Y H:i:s', $record[$f['field']]);
							}
							break;
		
							case 'MONEY':
							case 'DECIMAL':
							case 246:
							$record[$f['field']] = (float)$a1[$f['field']];
							$record['des'.$f['field']] = number_format($record[$f['field']],2,',','.');
							break;

							default:
							$value = ($this->utf8 === true)?utf8_encode(trim($a1[$f['field']])):trim($a1[$f['field']]);
							$record[$f['field']] = $value;
							unset($value);
							break;
						}
							
					}
					
					array_push($data, $record);
						
				}

			break;

		}

		return $data;

	}

	public function arrays($query, $array = false, $params = array()){

		$data = $this->getArrayRows($this->query($query, $params));
		
		if(!$array){
			return $data;	
		}else{		
			if(count($data)==1 && $array){
				return $data[0];
			}else{
				return $data;
			}
		}
			
	}

	public function insert($query, $params = array()){
		
		return $this->select($query, $params);
		
	}

	public function proc($name, $params = array(), $returnQuery = false){

		if($this->getType() === Sql::MYSQL){

			$i = array();
			foreach($params as $p){
				array_push($i, "?");
			}
			$query = "CALL ".$name."(".implode(", ", $i).");";

		}

		if($returnQuery === false){
			return $this->arrays($query, false, $params);
		}else{
			return $query;
		}

	}

	public function __destruct(){

		mysqli_close($this->conn);

	}

}

?>