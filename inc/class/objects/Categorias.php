<?php

class Categorias extends Collection {

	protected $class = "Categoria";
	protected $saveQuery = "CALL sp_lugarcategoria_save(?, ?)";
	protected $saveArgs = array("idcategoria", "descategoria");
	protected $pk = "idcategoria";

	public function get(){}

	public static function listAll(){

		$categorias = new Categorias();

		$categorias->loadFromQuery("SELECT * FROM v_lugarescategorias");

		return $categorias;

	}

}

?>