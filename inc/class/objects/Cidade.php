<?php

class Cidade extends Model{

	public $required = array("idestado", "descidade");
	public $pk = "idcidade";

	public function get(){

		$args = func_get_args();
		if(!isset($args[0])) throw new Exception($this->pk." não informado.");

		$this->queryToAttr("CALL sp_cidade_get(".$args[0].");");

	}

	public function save(){

		if($this->getChanged() && $this->isValid()){

			$this->queryToAttr("CALL sp_cidade_save(?, ?, ?);", array(
				$this->getidcidade(),
				$this->getidestado(),
				$this->getdescidade()
			));

			return $this->getidcidade();

		}else{

			return false;

		}

	}

	public function remove(){

		$this->execute("CALL sp_cidade_remove(".$this->getidcidade().";)");

		return true;

	}

}

?>