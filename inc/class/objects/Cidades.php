<?php

class Cidades extends Collection {

	protected $class = "Cidade";
	protected $saveQuery = "CALL sp_cidade_save(?, ?, ?)";
	protected $saveArgs = array("idcidade", "idestado", "descidade");
	protected $pk = "idcidade";

	public function get(){}

	public static function listAll(){

		$cidades = new Cidades();

		$cidades->loadFromQuery("SELECT * FROM v_cidades");

		return $cidades;

	}

}

?>