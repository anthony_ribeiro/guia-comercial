<?php

class Estado extends Model{

	public $required = array("desestado");
	public $pk = "idestado";

	public function get(){

		$args = func_get_args();
		if(!isset($args[0])) throw new Exception($this->pk." não informado.");

		$this->queryToAttr("CALL sp_estado_get(".$args[0].");");

	}

	public function save(){

		if($this->getChanged() && $this->isValid()){

			$this->queryToAttr("CALL sp_estado_save(?, ?, ?);", array(
				$this->getidestado(),
				$this->getdesestado(),
				$this->getdessigla()
			));

			return $this->getidestado();

		}else{

			return false;

		}

	}

	public function remove(){

		$this->execute("CALL sp_estado_remove(".$this->getidestado().";)");

		return true;

	}

}

?>