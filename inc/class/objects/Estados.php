<?php

class Estados extends Collection {

	protected $class = "Estado";
	protected $saveQuery = "CALL sp_estado_save(?, ?, ?)";
	protected $saveArgs = array("idestado", "desestado", "dessigla");
	protected $pk = "idestado";

	public function get(){}

	public static function listAll(){

		$estados = new Estados();

		$estados->loadFromQuery("SELECT * FROM v_estados");

		return $estados;

	}

}

?>