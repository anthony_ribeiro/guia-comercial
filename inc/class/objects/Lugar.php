<?php

class Lugar extends Model{

	public $required = array("idlugartipo", "idcidade", "deslugar");
	public $pk = "idlugar";

	public function get(){

		$args = func_get_args();
		if(!isset($args[0])) throw new Exception($this->pk." não informado.");

		$this->queryToAttr("CALL sp_lugar_get(".$args[0].");");

	}

	public function save(){

		if($this->getChanged() && $this->isValid()){

			$this->queryToAttr("CALL sp_lugar_save(?, ?, ?, ?, ?, ?, ?, ?, ?, ?);", array(
					$this->getidlugar(),
					$this->getidlugartipo(),
					$this->getidcidade(),
					$this->getdeslugar(),
					$this->getdesendereco(),
					$this->getdescep(),
					$this->getdesdescricao(),
					$this->getdeslatitude(),
					$this->getdeslongitude(),
					$this->getdeslogo()
			));

			return $this->getidlugar();

		}else{

			return false;

		}

	}

	public function remove(){

		$this->execute("CALL sp_lugar_remove(".$this->getidlugar().";)");

		return true;

	}

}

?>