<?php

class Lugares extends Collection {

	protected $class = "Lugar";
	protected $saveQuery = "CALL sp_lugar_save(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	protected $saveArgs = array("idlugar", "idlugartipo", "idcidade", "deslugar", "desendereco", "descep", "desdescricao", "deslatitude", "deslongitude", "deslogo");
	protected $pk = "idlugar";

	public function get(){}

	public static function listAll(){

		$lugares = new Lugares();

		$lugares->loadFromQuery("SELECT * FROM v_lugares");

		return $lugares;

	}

	public static function getFotos($idlugar){

		$fotos = new Lugares();

		$fotos->loadFromQuery("CALL sp_lugarfoto_get(".(int)$idlugar.")");

		return $fotos;

	}

	public static function getContatos($idlugar){

		$contatos = new Lugares();

		$contatos->loadFromQuery("CALL sp_lugarcontato_get(".(int)$idlugar.")");

		return $contatos;

	}

}

?>