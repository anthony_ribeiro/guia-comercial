<?php

class Pessoas extends Collection {

	protected $class = "Pessoa";
	protected $saveQuery = "CALL sp_pessoa_save(?, ?, ?)";
	protected $saveArgs = array("idpessoa", "idpessoatipo", "despessoa");
	protected $pk = "idpessoa";

	public function get(){}

	public static function listAll(){

		$pessoas = new Pessoas();

		$pessoas->loadFromQuery("SELECT * FROM v_pessoas");

		return $pessoas;

	}

}

?>