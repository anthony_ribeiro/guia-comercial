<?php

class Usuario extends Model{

	public $required = array("idpessoa", "desusuario");
	public $pk = "idusuario";

	public function get(){

		$args = func_get_args();
		if(!isset($args[0])) throw new Exception($this->pk." não informado.");

		$this->queryToAttr("CALL sp_usuario_get(".$args[0].");");

	}

	public function save(){

		if($this->getChanged() && $this->isValid()){

			$this->queryToAttr("CALL sp_usuario_save(?, ?, ?, ?);", array(
				$this->getidusuario(),
				$this->getidpessoa(),
				$this->getdesusuario(),
				$this->getdessenha()
			));

			return $this->getidusuario();

		}else{

			return false;

		}

	}

	public function remove(){

		$this->execute("CALL sp_usuario_remove(".$this->getidusuario().";)");

		return true;

	}

}

?>