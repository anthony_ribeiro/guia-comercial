<?php

class Usuarios extends Collection {

	protected $class = "Usuario";
	protected $saveQuery = "CALL sp_usuario_save(?, ?, ?, ?)";
	protected $saveArgs = array("idusuario", "idpessoa", "desusuario", "dessenha");
	protected $pk = "idusuario";

	public function get(){}

	public static function listAll(){

		$usuarios = new Usuarios();

		$usuarios->loadFromQuery("SELECT * FROM v_usuarios");

		return $usuarios;

	}

}

?>