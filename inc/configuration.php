<?php

session_start();
date_default_timezone_set('America/Sao_paulo');
error_reporting(E_ALL);

if((float)PHP_VERSION < 5.3) define("__DIR__", dirname(__FILE__));

require_once("consts.php");
require_once("functions.php");
require_once("raintpl/inc/rain.tpl.class.php");

spl_autoload_register("autoload_php_default_project"); //Define a função php_default_autoload como a função de __autoload

?>