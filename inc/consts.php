<?php

// tipo de banco
define('DB_TYPE', (int)'1');

// host
define("HOST", "localhost");

// user
define("USER", "root");

// password
define("PASSWORD", "root");

// db
define("DB", "guia");

// path
define("PATH", realpath(__DIR__."/../"));

// URL
define("URL", str_replace("\\", "/", str_replace(PATH, "", str_replace("/", "\\", $_SERVER["SCRIPT_FILENAME"]))));

// Site PATH
define("SITE_PATH", str_replace($_SERVER['DOCUMENT_ROOT'], "", str_replace("\\", "/", PATH)));

?>