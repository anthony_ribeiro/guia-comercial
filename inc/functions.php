<?php

function autoload_php_default_project($class){
	$filepath = __DIR__.DIRECTORY_SEPARATOR."class".DIRECTORY_SEPARATOR.$class.".php";
    if(file_exists($filepath)){
    	require_once($filepath);
    	return true;
    }
    $filepath = __DIR__.DIRECTORY_SEPARATOR."class".DIRECTORY_SEPARATOR."objects".DIRECTORY_SEPARATOR.$class.".php";
    if(file_exists($filepath)){
    	require_once($filepath);
    	return true;
    }
}
if(URL === '/inc/configuration.php'){
	phpinfo();
	exit;
}
if(!function_exists('removeSimplesQuotes')){
	function removeSimplesQuotes($val){
		return str_replace("'", "", $val);
	}
}
if(!function_exists('post')){
	function post($key){
		return (isset($_POST[$key]))?removeSimplesQuotes($_POST[$key]):NULL;
	}
}
if(!function_exists('get')){
	function get($key){
		return (isset($_GET[$key]))?removeSimplesQuotes($_GET[$key]):NULL;
	}
}
if(!function_exists('pre')){
	function pre(){
		echo "<pre>";
		foreach(func_get_args() as $arg){
			print_r($arg);
		}
		echo "</pre>";
	}
}
if(!function_exists('success')){
	function success($data = array()){

		$json = json_encode(array_merge(array(
			'success'=>true,
			'delay'=>microtime(true)-START_EXECUTION
		), $data));

		if(isset($_GET['callback'])){
			return get('callback').'('.$json.')';
		}elseif(isset($_GET['jsonp'])){
			return get('jsonp').'('.$json.')';
		}else{
			return $json;
		}

	}
}
if(!function_exists('send_email')){
	function send_email($options, $tplname = "blank", $data = array(""), $comTemplatePadrao = true){

		/************************************************************/
		if(isset($options['body'])) $data['body'] = $options['body'];

		if(isset($_SESSION)) $data['data']['session'] = $_SESSION;
		if(isset($_SERVER)) $data['data']['server'] = $_SERVER;

		raintpl::configure("base_url", PATH );
		raintpl::configure("tpl_dir", PATH."/res/tpl/email/" );
		raintpl::configure("cache_dir", PATH."/res/tpl/tmp/" );
		raintpl::configure("path_replace", false );

		$tpl = new RainTPL;

		if(gettype($data)=='array'){
			foreach($data as $key=>$val){
				$tpl->assign($key, $val);
			}
		}	

		$body = "";
		if($comTemplatePadrao === true) $body .= $tpl->draw("header", true);
		$body .= $tpl->draw($tplname, true);
		if($comTemplatePadrao === true) $body .= $tpl->draw("footer", true);

		$options['body'] = $body;
		/************************************************************/
		$classpath = getClassPath().DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."PHPMailer".DIRECTORY_SEPARATOR;

		require_once($classpath."class.phpmailer.php");
		require_once($classpath."class.smtp.php");
		/************************************************************/
		$opt = array_merge(array(
			"username"=>"",
			"password"=>"",
			"Host"=>"",
			"SMTPAuth"=>true,
			"Port"=>25,
			"from"=>"",
			"fromName"=>"",
			"reply"=>false,
			"replayName"=>false,
			"AltBody"=>"Por favor, utilize um visualizador de e-mail compátivel com HTML para visuliazar esta mensagem.",
			"to"=>array(),
			"cc"=>array(),
			"cco"=>array(),
			"priority"=>3,
			"subject"=>"",
			"CharSet"=>"UTF-8"
		), $options);
		
		$mail             = new PHPMailer();
		$mail->IsSMTP();
		$mail->Host       = $opt['Host'];
		$mail->IsHTML();
		$mail->SMTPAuth   = $opt['SMTPAuth'];
		$mail->Port       = $opt['Port'];
		$mail->Username   = $opt['username'];
		$mail->Password   = $opt['password'];

		//$mail->SMTPSecure = "tls";
		if($opt['embeddedImage']){
			foreach($opt['embeddedImage'] as $embeddedImage){
				if($embeddedImage['path'] && $embeddedImage['id'])
				$mail->AddEmbeddedImage($embeddedImage['path'],$embeddedImage['id']);
			}	
		}

		if($opt['CharSet']) $mail->CharSet = $opt['CharSet'];
		
		$mail->Priority = $opt['priority'];
		
		$mail->SetFrom($opt['from'], $opt['fromName']);
		
		if($opt['reply']) $mail->AddReplyTo($opt['reply'],$opt['replayName']);
		
		$mail->Subject    = $opt['subject'];
		
		$mail->AltBody    = $opt['AltBody'];
		
		$mail->MsgHTML($opt['body']);
		
		if(is_array($opt['to'])){
			foreach($opt['to'] as $to){
				$mail->AddAddress($to['email'], $to['name']);
			}
		}else{
			foreach(explode(';', $opt['to']) as $email){
				$mail->AddAddress($email);
			}
		}
		
		if(is_array($opt['cc'])){
			foreach($opt['cc'] as $to){
				$mail->AddCC($to['email'], $to['name']);
			}
		}else{
			foreach(explode(';', $opt['cc']) as $email){
				$mail->AddCC($email);
			}
		}

		if(is_array($opt['cco'])){
			foreach($opt['cco'] as $to){
				$mail->AddBCC($to['email'], $to['name']);
			}
		}else{
			foreach(explode(';', $opt['cco']) as $email){
				$mail->AddBCC($email);
			}
		}
		
		if(!$mail->Send()){
		  return false;
		} else {
		  return true;
		}
		
	}
}
if(!function_exists("getLocationByIP")){
	function getLocationByIP(){
		// $location = file_get_contents('http://freegeoip.net/json/'.$_SERVER['HTTP_CLIENT_IP']);
		$location = json_decode(file_get_contents('http://freegeoip.net/json/8.8.8.8'),true);
		return array("latitude"=>$location["latitude"], "longitude"=>$location["longitude"]);
	}
}

?>