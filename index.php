<?php

define('START_EXECUTION', microtime(true));

require_once("inc/configuration.php");
require 'inc/Slim/Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

$app->get('/', function () {		
    
    $page = new Page();

    $page->setTpl("index");

});

$modules_path = __DIR__.DIRECTORY_SEPARATOR."modules".DIRECTORY_SEPARATOR;

if (!is_dir($modules_path)) {
    mkdir($modules_path);
}

foreach (scandir($modules_path) as $file) {

    if ($file !== '.' && $file !== '..') {
        require_once($modules_path.$file);
    }

}

$app->run();