<?php

$app->get("/cidades/all", function(){

	$cidades = Cidades::listAll();

	echo success(array("data"=>$cidades->getFields()));

});

$app->post("/cidades/:idcidade", function($idcidade){

	if(!(int)$idcidade){
		throw new Exception("ID de cidade não informado");		
	}

	$cidade = new Cidade((int)$idcidade);

	if(!(int)$cidade->getidcidade() > 0){
		throw new Exception("Cidade não encontrada");		
	}

	foreach($_POST as $key => $value){
		$cidade->{'set'.$key}(post($key));
	}

	$cidade->save();

	echo success(array("data"=>$cidade->getFields()));

});

$app->post("/cidades", function(){

	$cidade = new Cidade($_POST);
	$cidade->save();

});

$app->delete("/cidades/:idcidade", function($idcidade){

	if(!(int)$idcidade){
		throw new Exception("ID de cidade não informado");		
	}

	$cidade = new Cidade((int)$idcidade);

	if(!(int)$cidade->getidcidade() > 0){
		throw new Exception("Cidade não encontrada");		
	}

	$cidade->remove();

	echo success();

});

$app->delete("/cidades", function(){

	$ids = explode(",", post("ids"));

	foreach($ids as $idcidade){

		if(!(int)$idcidade){
			throw new Exception("ID de cidade não informado");		
		}

		$cidade = new Cidade((int)$idcidade);

		if(!(int)$cidade->getidcidade() > 0){
			throw new Exception("Cidade não encontrada");		
		}

		$cidade->remove();

	}

	echo success();

});

?>