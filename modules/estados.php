<?php

$app->get("/estados/all", function(){

	$estados = Estados::listAll();

	echo success(array("data"=>$estados->getFields()));

});

$app->post("/estados/:idestado", function($idestado){

	if(!(int)$idestado){
		throw new Exception("ID de estado não informado");		
	}

	$estado = new Estado((int)$idestado);

	if(!(int)$estado->getidestado() > 0){
		throw new Exception("Estado não encontrado");		
	}

	foreach($_POST as $key => $value){
		$estado->{'set'.$key}(post($key));
	}

	$estado->save();

	echo success(array("data"=>$estado->getFields()));

});

$app->post("/estados", function(){

	$estado = new Estado($_POST);
	$estado->save();

	echo success(array("data"=>$estado->getFields()));

});

$app->delete("/estados/:idestado", function($idestado){

	if(!(int)$idestado){
		throw new Exception("ID de estado não informado");		
	}

	$estado = new Estado((int)$idestado);

	if(!(int)$estado->getidestado() > 0){
		throw new Exception("Estado não encontrado");		
	}

	$estado->remove();

	echo success();

});

$app->delete("/estados", function(){

	$ids = explode(",", post("ids"));

	foreach($ids as $idestado){

		if(!(int)$idestado){
			throw new Exception("ID de estado não informado");		
		}

		$estado = new Estado((int)$idestado);

		if(!(int)$estado->getidestado() > 0){
			throw new Exception("Estado não encontrado");		
		}

		$estado->remove();

	}

	echo success();

});

?>