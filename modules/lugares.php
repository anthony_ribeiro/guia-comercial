<?php

$app->get("/lugares/all", function(){

	$lugares = Lugares::listAll();

	echo success(array("data"=>$lugares->getFields()));

});

$app->get("/lugares/categorias/all", function(){

	$categorias = Categorias::listAll();

	echo success(array("data"=>$categorias->getFields()));

});

$app->get("/categorias/:idcategoria/lugares", function($idcategoria){

	if(!(int)$idcategoria > 0){
		throw new Exception("ID de categoria não informado");		
	}

	$categoria = new Categoria((int)$idcategoria);

	// if(!(int)$categoria->getidcategoria() > 0){
	// 	throw new Exception("Categoria não encontrada");		
	// } não entendi

	// var_dump($categoria->getFields());
	// exit;

	$page = new Page();

	$page->setTpl("lugares", array(
		"lugares"=>$categoria->getFields()
	));

});

$app->get("/lugares/:idlugar", function($idlugar){

	if(!(int)$idlugar > 0){
		throw new Exception("ID de lugar não informado");		
	}

	$lugar = new Lugar((int)$idlugar);

	if(!(int)$lugar->getidlugar() > 0){
		throw new Exception("Lugar não encontrado");		
	}

	$fotos = Lugares::getFotos((int)$idlugar);	
	// echo json_encode($lugar->getFields());
	// exit;

	$page = new Page();

	$page->setTpl("lugar", array(
		"lugar"=>$lugar->getFields(),
		"fotos"=>$fotos->getFields()
	));

});

$app->get("/lugares/:idlugar/contatos/all", function($idlugar){

	if(!(int)$idlugar > 0){
		throw new Exception("ID de lugar não informado");	
	}

	$contatos = Lugares::getContatos((int)$idlugar);

	echo success(array(
		"data"=>$contatos->getFields()
	));

});

$app->get("/lugares/:idlugar/fotos/all", function($idlugar){

	if(!(int)$idlugar > 0){
		throw new Exception("ID de lugar não informado");	
	}

	$fotos = Lugares::getFotos((int)$idlugar);

	echo success(array(
		"data"=>$fotos->getFields()
	));

});

$app->post("/lugares/:idlugar", function($idlugar){

	if(!(int)$idlugar){
		throw new Exception("ID de lugar não informado");		
	}

	$lugar = new Lugar((int)$idlugar);

	if(!(int)$lugar->getidlugar() > 0){
		throw new Exception("Lugar não encontrado");		
	}

	foreach($_POST as $key => $value){
		$lugar->{'set'.$key}(post($key));
	}

	$lugar->save();

	echo success(array("data"=>$lugar->getFields()));

});

$app->post("/lugares", function(){

	$usuario = new Usuario($_POST);
	$usuario->save();

	echo success(array("data"=>$usuario->getFields()));

});

$app->delete("/lugares/:idlugar", function($idlugar){

	if(!(int)$idlugar){
		throw new Exception("ID de lugar não informado");		
	}

	$lugar = new Lugar((int)$idlugar);

	if(!(int)$lugar->getidlugar() > 0){
		throw new Exception("Lugar não encontrado");		
	}

	$lugar->remove();

	echo success();

});

$app->delete("/lugares", function(){

	$ids = explode(",", post("ids"));

	foreach($ids as $idlugar){

		if(!(int)$idlugar){
			throw new Exception("ID de lugar não informado");			
		}

		$lugar = new Lugar((int)$idlugar);

		if(!(int)$lugar->getidlugar() > 0){
			throw new Exception("Lugar não encontrado");			
		}

		$lugar->remove();

	}

	echo success();

});

?>