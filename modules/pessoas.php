<?php

$app->get("/pessoas/all", function(){

	$pessoas = Pessoas::listAll();

	// while($registro = mysqli_fetch_assoc($result)){
	// 	echo json_encode(array(
	// 		"success"=>true,
	// 		"data"=>$registro
	// 	));
	// } //Não exibe os resultados

	echo success(array("data"=>$pessoas->getFields()));

});

$app->post("/pessoas/:idpessoa", function($idpessoa){

	if(!(int)$idpessoa){
		throw new Exception("ID de pessoa não informado");		
	}

	$pessoa = new Pessoa((int)$idpessoa);

	if(!(int)$pessoa->getidpessoa() > 0){
		throw new Exception("Pessoa não encontrada");		
	}

	foreach($_POST as $key => $value){
		$pessoa->{'set'.$key}(post($key));
	}

	$pessoa->save();

	echo success(array("data"=>$pessoa->getFields()));

});

$app->post("/pessoas", function(){

	$pessoa = new Pessoa($_POST);
	$pessoa->save();

	echo success(array("data"=>$pessoa->getFields()));

});

$app->delete("/pessoas/:idpessoa", function($idpessoa){

	if(!(int)$idpessoa){
		throw new Exception("ID de pessoa não informado");	
	}

	$pessoa = new Pessoa((int)$idpessoa);

	if(!(int)$pessoa->getidpessoa() > 0){
		throw new Exception("Pessoa não encontrada", 404);		
	}

	$pessoa->remove();

	echo success();

});

$app->delete("/pessoas", function(){

	$ids = explode(",", post("ids"));

	foreach($ids as $idpessoa){

		if(!(int)$idpessoa > 0){
			throw new Exception("ID de pessoa não informado");			
		}

		$pessoa = new Pessoa((int)$idpessoa);

		if(!(int)$pessoa->getidpessoa() > 0){
			throw new Exception("Pessoa não encontrada.");			
		}

		$pessoa->remove();

	}

	echo success();

});

?>