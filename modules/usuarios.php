<?php

$app->get("/usuarios/all", function(){

	$usuarios = Usuarios::listAll();

	echo success(array("data"=>$usuarios->getFields()));

});

$app->post("/usuarios/:idusuario", function($idusuario){

	if(!(int)$idusuario) throw new Exception("ID de usuário não informado");

	$usuario = new Usuario((int)$idusuario);

	if(!(int)$usuario->getidusuario() > 0){
		throw new Exception("Usuário não encontrado");		
	}

	foreach($_POST as $key => $value){
		$usuario->{'set'.$key}(post($key));
	}

	$usuario->save();

	echo success(array("data"=>$usuario->getFields()));

});

$app->post("/usuarios", function(){

	$usuario = new Usuario();
	$usuario->save();

	echo success(array("data"=>$usuario->getFields()));

});

$app->delete("/usuarios/:idusuario", function($idusuario){

	if(!(int)$idusuario) throw new Exception("ID de usuário não informado");

	$usuario = new Usuario((int)$idusuario);

	if(!(int)$usuario->getidusuario() > 0){
		throw new Exception("Usuário não encontrado");		
	}

	$usuario->remove();

	echo success();

});

$app->delete("/usuarios", function(){

	$ids = explode(",", post("ids"));

	foreach($ids as $idusuario){

		if(!(int)$idusuario) throw new Exception("ID de usuário não informado");

		$usuario = new Usuario((int)$idusuario);

		if(!(int)$usuario->getidusuario() > 0){
			throw new Exception("Usuário não encontrado");		
		}

		$usuario->remove();

	}

	echo success();

});

?>