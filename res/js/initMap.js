function initMap(){
	var lat = $("#map").data("lat");
	var lng = $("#map").data("lng");
	var title = $("#map").data("title");
	var mapDiv = document.getElementById("map");
	var myLatlng = {lat: lat, lng: lng};

	var map = new google.maps.Map(mapDiv, {
		center: myLatlng,
		zoom: 16
	});

	var marker = new google.maps.Marker({
	    position: myLatlng,
	    title: title
	});

	marker.setMap(map);
};