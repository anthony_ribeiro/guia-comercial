CREATE DATABASE  IF NOT EXISTS `guia` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `guia`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win32 (AMD64)
--
-- Host: 127.0.0.1    Database: guia
-- ------------------------------------------------------
-- Server version	5.5.47

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tb_cidades`
--

DROP TABLE IF EXISTS `tb_cidades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_cidades` (
  `idcidade` int(11) NOT NULL AUTO_INCREMENT,
  `idestado` int(11) NOT NULL,
  `descidade` varchar(128) NOT NULL,
  `dtcadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idcidade`),
  KEY `idestado` (`idestado`),
  CONSTRAINT `tb_cidades_ibfk_1` FOREIGN KEY (`idestado`) REFERENCES `tb_estados` (`idestado`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_cidades`
--

LOCK TABLES `tb_cidades` WRITE;
/*!40000 ALTER TABLE `tb_cidades` DISABLE KEYS */;
INSERT INTO `tb_cidades` VALUES (1,2,'Diamantina','2016-08-18 17:11:57'),(2,1,'São Bernardo do Campo','2016-08-18 17:11:57');
/*!40000 ALTER TABLE `tb_cidades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_contatostipos`
--

DROP TABLE IF EXISTS `tb_contatostipos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_contatostipos` (
  `idcontatotipo` int(11) NOT NULL AUTO_INCREMENT,
  `descontatotipo` varchar(128) NOT NULL,
  `dtcadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idcontatotipo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_contatostipos`
--

LOCK TABLES `tb_contatostipos` WRITE;
/*!40000 ALTER TABLE `tb_contatostipos` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_contatostipos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_estados`
--

DROP TABLE IF EXISTS `tb_estados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_estados` (
  `idestado` int(11) NOT NULL AUTO_INCREMENT,
  `desestado` varchar(128) NOT NULL,
  `dessigla` varchar(2) NOT NULL,
  `dtcadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idestado`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_estados`
--

LOCK TABLES `tb_estados` WRITE;
/*!40000 ALTER TABLE `tb_estados` DISABLE KEYS */;
INSERT INTO `tb_estados` VALUES (1,'São Paulo','SP','2016-08-18 17:07:56'),(2,'Minas Gerais','MG','2016-08-18 17:07:56');
/*!40000 ALTER TABLE `tb_estados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_fotos`
--

DROP TABLE IF EXISTS `tb_fotos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_fotos` (
  `idfoto` int(11) NOT NULL AUTO_INCREMENT,
  `desurl` varchar(128) NOT NULL,
  `dtcadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idfoto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_fotos`
--

LOCK TABLES `tb_fotos` WRITE;
/*!40000 ALTER TABLE `tb_fotos` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_fotos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_lugares`
--

DROP TABLE IF EXISTS `tb_lugares`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_lugares` (
  `idlugar` int(11) NOT NULL AUTO_INCREMENT,
  `idlugartipo` int(11) NOT NULL,
  `idcidade` int(11) NOT NULL,
  `deslugar` varchar(128) NOT NULL,
  `desendereco` varchar(128) NOT NULL,
  `descep` int(32) NOT NULL,
  `deslogo` varchar(128) NOT NULL,
  `dtcadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idlugar`),
  KEY `idlugartipo` (`idlugartipo`),
  KEY `idcidade` (`idcidade`),
  CONSTRAINT `tb_lugares_ibfk_1` FOREIGN KEY (`idlugartipo`) REFERENCES `tb_lugarestipos` (`idlugartipo`),
  CONSTRAINT `tb_lugares_ibfk_2` FOREIGN KEY (`idcidade`) REFERENCES `tb_cidades` (`idcidade`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_lugares`
--

LOCK TABLES `tb_lugares` WRITE;
/*!40000 ALTER TABLE `tb_lugares` DISABLE KEYS */;
INSERT INTO `tb_lugares` VALUES (1,4,1,'Cordeiro Supermercado','endereco',9854410,'cordeiro_logo','2016-08-18 01:35:16');
/*!40000 ALTER TABLE `tb_lugares` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_lugarescontatos`
--

DROP TABLE IF EXISTS `tb_lugarescontatos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_lugarescontatos` (
  `idcontato` int(11) NOT NULL AUTO_INCREMENT,
  `idlugar` int(11) NOT NULL,
  `idcontatotipo` int(11) NOT NULL,
  `descontato` varchar(64) NOT NULL,
  `dtcadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idcontato`),
  KEY `idlugar` (`idlugar`),
  KEY `idcontatotipo` (`idcontatotipo`),
  CONSTRAINT `tb_lugarescontatos_ibfk_1` FOREIGN KEY (`idlugar`) REFERENCES `tb_lugares` (`idlugar`),
  CONSTRAINT `tb_lugarescontatos_ibfk_2` FOREIGN KEY (`idcontatotipo`) REFERENCES `tb_contatostipos` (`idcontatotipo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_lugarescontatos`
--

LOCK TABLES `tb_lugarescontatos` WRITE;
/*!40000 ALTER TABLE `tb_lugarescontatos` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_lugarescontatos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_lugaresfotos`
--

DROP TABLE IF EXISTS `tb_lugaresfotos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_lugaresfotos` (
  `idlugar` int(11) NOT NULL,
  `idfoto` int(11) NOT NULL,
  `dtcadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `idlugar` (`idlugar`),
  KEY `idfoto` (`idfoto`),
  CONSTRAINT `tb_lugaresfotos_ibfk_1` FOREIGN KEY (`idlugar`) REFERENCES `tb_lugares` (`idlugar`),
  CONSTRAINT `tb_lugaresfotos_ibfk_2` FOREIGN KEY (`idfoto`) REFERENCES `tb_fotos` (`idfoto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_lugaresfotos`
--

LOCK TABLES `tb_lugaresfotos` WRITE;
/*!40000 ALTER TABLE `tb_lugaresfotos` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_lugaresfotos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_lugarestipos`
--

DROP TABLE IF EXISTS `tb_lugarestipos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_lugarestipos` (
  `idlugartipo` int(11) NOT NULL AUTO_INCREMENT,
  `deslugartipo` varchar(128) NOT NULL,
  `dtcadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idlugartipo`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_lugarestipos`
--

LOCK TABLES `tb_lugarestipos` WRITE;
/*!40000 ALTER TABLE `tb_lugarestipos` DISABLE KEYS */;
INSERT INTO `tb_lugarestipos` VALUES (1,'Restaurante','2016-08-18 01:32:23'),(2,'Loja','2016-08-18 01:32:23'),(3,'Posto de Gasolina','2016-08-18 01:32:23'),(4,'Supermercado','2016-08-18 01:32:23');
/*!40000 ALTER TABLE `tb_lugarestipos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-18 14:15:26
