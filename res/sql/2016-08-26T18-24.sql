CREATE DATABASE  IF NOT EXISTS `guia` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `guia`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win32 (AMD64)
--
-- Host: 127.0.0.1    Database: guia
-- ------------------------------------------------------
-- Server version	5.5.47

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tb_cidades`
--

DROP TABLE IF EXISTS `tb_cidades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_cidades` (
  `idcidade` int(11) NOT NULL AUTO_INCREMENT,
  `idestado` int(11) NOT NULL,
  `descidade` varchar(128) NOT NULL,
  `dtcadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idcidade`),
  KEY `idestado` (`idestado`),
  CONSTRAINT `tb_cidades_ibfk_1` FOREIGN KEY (`idestado`) REFERENCES `tb_estados` (`idestado`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_cidades`
--

LOCK TABLES `tb_cidades` WRITE;
/*!40000 ALTER TABLE `tb_cidades` DISABLE KEYS */;
INSERT INTO `tb_cidades` VALUES (1,2,'Diamantina','2016-08-18 17:11:57'),(2,1,'São Bernardo do Campo','2016-08-18 17:11:57');
/*!40000 ALTER TABLE `tb_cidades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_contatostipos`
--

DROP TABLE IF EXISTS `tb_contatostipos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_contatostipos` (
  `idcontatotipo` int(11) NOT NULL AUTO_INCREMENT,
  `descontatotipo` varchar(128) NOT NULL,
  `dtcadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idcontatotipo`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_contatostipos`
--

LOCK TABLES `tb_contatostipos` WRITE;
/*!40000 ALTER TABLE `tb_contatostipos` DISABLE KEYS */;
INSERT INTO `tb_contatostipos` VALUES (1,'Telefone','2016-08-18 20:45:37'),(2,'WhatsApp','2016-08-18 20:45:37'),(3,'Email','2016-08-18 20:45:37');
/*!40000 ALTER TABLE `tb_contatostipos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_estados`
--

DROP TABLE IF EXISTS `tb_estados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_estados` (
  `idestado` int(11) NOT NULL AUTO_INCREMENT,
  `desestado` varchar(128) NOT NULL,
  `dessigla` varchar(2) NOT NULL,
  `dtcadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idestado`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_estados`
--

LOCK TABLES `tb_estados` WRITE;
/*!40000 ALTER TABLE `tb_estados` DISABLE KEYS */;
INSERT INTO `tb_estados` VALUES (1,'São Paulo','SP','2016-08-18 17:07:56'),(2,'Minas Gerais','MG','2016-08-18 17:07:56');
/*!40000 ALTER TABLE `tb_estados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_fotos`
--

DROP TABLE IF EXISTS `tb_fotos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_fotos` (
  `idfoto` int(11) NOT NULL AUTO_INCREMENT,
  `desurl` varchar(128) NOT NULL,
  `dtcadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idfoto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_fotos`
--

LOCK TABLES `tb_fotos` WRITE;
/*!40000 ALTER TABLE `tb_fotos` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_fotos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_lugares`
--

DROP TABLE IF EXISTS `tb_lugares`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_lugares` (
  `idlugar` int(11) NOT NULL AUTO_INCREMENT,
  `idlugartipo` int(11) NOT NULL,
  `idcidade` int(11) NOT NULL,
  `deslugar` varchar(128) NOT NULL,
  `desendereco` varchar(128) NOT NULL,
  `descep` varchar(20) DEFAULT NULL,
  `desdescricao` varchar(256) NOT NULL,
  `deslatitude` decimal(10,7) DEFAULT NULL,
  `deslongitude` decimal(10,7) DEFAULT NULL,
  `deslogo` varchar(128) DEFAULT 'logo.png',
  `dtcadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idlugar`),
  KEY `idlugartipo` (`idlugartipo`),
  KEY `idcidade` (`idcidade`),
  CONSTRAINT `tb_lugares_ibfk_1` FOREIGN KEY (`idlugartipo`) REFERENCES `tb_lugarestipos` (`idlugartipo`),
  CONSTRAINT `tb_lugares_ibfk_2` FOREIGN KEY (`idcidade`) REFERENCES `tb_cidades` (`idcidade`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_lugares`
--

LOCK TABLES `tb_lugares` WRITE;
/*!40000 ALTER TABLE `tb_lugares` DISABLE KEYS */;
INSERT INTO `tb_lugares` VALUES (1,4,1,'Cordeiro Supermercado','endereco','9854410','',NULL,NULL,'cordeiro_logo.png','2016-08-26 19:24:43');
/*!40000 ALTER TABLE `tb_lugares` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_lugarescontatos`
--

DROP TABLE IF EXISTS `tb_lugarescontatos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_lugarescontatos` (
  `idcontato` int(11) NOT NULL AUTO_INCREMENT,
  `idlugar` int(11) NOT NULL,
  `idcontatotipo` int(11) NOT NULL,
  `descontato` varchar(64) NOT NULL,
  `dtcadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idcontato`),
  KEY `idlugar` (`idlugar`),
  KEY `idcontatotipo` (`idcontatotipo`),
  CONSTRAINT `tb_lugarescontatos_ibfk_1` FOREIGN KEY (`idlugar`) REFERENCES `tb_lugares` (`idlugar`),
  CONSTRAINT `tb_lugarescontatos_ibfk_2` FOREIGN KEY (`idcontatotipo`) REFERENCES `tb_contatostipos` (`idcontatotipo`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_lugarescontatos`
--

LOCK TABLES `tb_lugarescontatos` WRITE;
/*!40000 ALTER TABLE `tb_lugarescontatos` DISABLE KEYS */;
INSERT INTO `tb_lugarescontatos` VALUES (1,1,1,'4342-8310','2016-08-18 20:45:53'),(2,1,2,'98823-3571','2016-08-18 21:04:46');
/*!40000 ALTER TABLE `tb_lugarescontatos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_lugaresfotos`
--

DROP TABLE IF EXISTS `tb_lugaresfotos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_lugaresfotos` (
  `idlugar` int(11) NOT NULL,
  `idfoto` int(11) NOT NULL,
  `dtcadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `idlugar` (`idlugar`),
  KEY `idfoto` (`idfoto`),
  CONSTRAINT `tb_lugaresfotos_ibfk_1` FOREIGN KEY (`idlugar`) REFERENCES `tb_lugares` (`idlugar`),
  CONSTRAINT `tb_lugaresfotos_ibfk_2` FOREIGN KEY (`idfoto`) REFERENCES `tb_fotos` (`idfoto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_lugaresfotos`
--

LOCK TABLES `tb_lugaresfotos` WRITE;
/*!40000 ALTER TABLE `tb_lugaresfotos` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_lugaresfotos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_lugarestipos`
--

DROP TABLE IF EXISTS `tb_lugarestipos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_lugarestipos` (
  `idlugartipo` int(11) NOT NULL AUTO_INCREMENT,
  `deslugartipo` varchar(128) NOT NULL,
  `dtcadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idlugartipo`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_lugarestipos`
--

LOCK TABLES `tb_lugarestipos` WRITE;
/*!40000 ALTER TABLE `tb_lugarestipos` DISABLE KEYS */;
INSERT INTO `tb_lugarestipos` VALUES (1,'Restaurante','2016-08-18 01:32:23'),(2,'Loja','2016-08-18 01:32:23'),(3,'Posto de Gasolina','2016-08-18 01:32:23'),(4,'Supermercado','2016-08-18 01:32:23');
/*!40000 ALTER TABLE `tb_lugarestipos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_permissoes`
--

DROP TABLE IF EXISTS `tb_permissoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_permissoes` (
  `idpermissao` int(11) NOT NULL AUTO_INCREMENT,
  `despermissao` varchar(32) NOT NULL,
  `dtcadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idpermissao`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_permissoes`
--

LOCK TABLES `tb_permissoes` WRITE;
/*!40000 ALTER TABLE `tb_permissoes` DISABLE KEYS */;
INSERT INTO `tb_permissoes` VALUES (1,'Super Usuário','2016-08-21 02:01:38'),(2,'Área Administrativa','2016-08-21 02:01:38'),(3,'Área Restrita','2016-08-21 02:01:38');
/*!40000 ALTER TABLE `tb_permissoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_permissoesusuarios`
--

DROP TABLE IF EXISTS `tb_permissoesusuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_permissoesusuarios` (
  `idpermissao` int(11) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `dtcadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `idpermissao` (`idpermissao`),
  KEY `idusuario` (`idusuario`),
  CONSTRAINT `tb_permissoesusuarios_ibfk_1` FOREIGN KEY (`idpermissao`) REFERENCES `tb_permissoes` (`idpermissao`),
  CONSTRAINT `tb_permissoesusuarios_ibfk_2` FOREIGN KEY (`idusuario`) REFERENCES `tb_usuarios` (`idusuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_permissoesusuarios`
--

LOCK TABLES `tb_permissoesusuarios` WRITE;
/*!40000 ALTER TABLE `tb_permissoesusuarios` DISABLE KEYS */;
INSERT INTO `tb_permissoesusuarios` VALUES (1,1,'2016-08-21 02:12:53'),(1,2,'2016-08-21 02:12:53');
/*!40000 ALTER TABLE `tb_permissoesusuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_pessoas`
--

DROP TABLE IF EXISTS `tb_pessoas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_pessoas` (
  `idpessoa` int(11) NOT NULL AUTO_INCREMENT,
  `idpessoatipo` int(11) NOT NULL,
  `despessoa` varchar(128) NOT NULL,
  `dtcadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idpessoa`),
  KEY `idpessoatipo` (`idpessoatipo`),
  CONSTRAINT `tb_pessoas_ibfk_1` FOREIGN KEY (`idpessoatipo`) REFERENCES `tb_pessoastipos` (`idpessoatipo`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_pessoas`
--

LOCK TABLES `tb_pessoas` WRITE;
/*!40000 ALTER TABLE `tb_pessoas` DISABLE KEYS */;
INSERT INTO `tb_pessoas` VALUES (2,1,'Anthony Ribeiro','2016-08-21 02:08:28'),(3,1,'João Rangel','2016-08-21 02:08:28'),(5,1,'Djalma','2016-08-26 21:21:42');
/*!40000 ALTER TABLE `tb_pessoas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_pessoastipos`
--

DROP TABLE IF EXISTS `tb_pessoastipos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_pessoastipos` (
  `idpessoatipo` int(11) NOT NULL AUTO_INCREMENT,
  `despessoatipo` varchar(128) NOT NULL,
  `dtcadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idpessoatipo`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_pessoastipos`
--

LOCK TABLES `tb_pessoastipos` WRITE;
/*!40000 ALTER TABLE `tb_pessoastipos` DISABLE KEYS */;
INSERT INTO `tb_pessoastipos` VALUES (1,'Pessoa Física','2016-08-21 02:00:48'),(2,'Pessoa Jurídica','2016-08-21 02:00:48');
/*!40000 ALTER TABLE `tb_pessoastipos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_usuarios`
--

DROP TABLE IF EXISTS `tb_usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_usuarios` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `idpessoa` int(11) NOT NULL,
  `desusuario` varchar(128) NOT NULL,
  `dessenha` varchar(64) NOT NULL,
  `inbloqueado` bit(1) DEFAULT NULL,
  `dtcadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idusuario`),
  KEY `idpessoa` (`idpessoa`),
  CONSTRAINT `tb_usuarios_ibfk_1` FOREIGN KEY (`idpessoa`) REFERENCES `tb_pessoas` (`idpessoa`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_usuarios`
--

LOCK TABLES `tb_usuarios` WRITE;
/*!40000 ALTER TABLE `tb_usuarios` DISABLE KEYS */;
INSERT INTO `tb_usuarios` VALUES (1,2,'anthonyribeiro','14789632',NULL,'2016-08-21 02:12:28'),(2,3,'joaohcrangel','1914',NULL,'2016-08-21 02:12:28');
/*!40000 ALTER TABLE `tb_usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `v_cidades`
--

DROP TABLE IF EXISTS `v_cidades`;
/*!50001 DROP VIEW IF EXISTS `v_cidades`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_cidades` AS SELECT 
 1 AS `idcidade`,
 1 AS `descidade`,
 1 AS `desestado`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_estados`
--

DROP TABLE IF EXISTS `v_estados`;
/*!50001 DROP VIEW IF EXISTS `v_estados`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_estados` AS SELECT 
 1 AS `idestado`,
 1 AS `desestado`,
 1 AS `dessigla`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_lugares`
--

DROP TABLE IF EXISTS `v_lugares`;
/*!50001 DROP VIEW IF EXISTS `v_lugares`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_lugares` AS SELECT 
 1 AS `idlugar`,
 1 AS `deslugar`,
 1 AS `desendereco`,
 1 AS `descep`,
 1 AS `desdescricao`,
 1 AS `deslatitude`,
 1 AS `deslongitude`,
 1 AS `deslogo`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_pessoas`
--

DROP TABLE IF EXISTS `v_pessoas`;
/*!50001 DROP VIEW IF EXISTS `v_pessoas`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_pessoas` AS SELECT 
 1 AS `idpessoa`,
 1 AS `despessoa`,
 1 AS `despessoatipo`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_usuarios`
--

DROP TABLE IF EXISTS `v_usuarios`;
/*!50001 DROP VIEW IF EXISTS `v_usuarios`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_usuarios` AS SELECT 
 1 AS `idusuario`,
 1 AS `desusuario`,
 1 AS `dessenha`,
 1 AS `despessoa`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `v_cidades`
--

/*!50001 DROP VIEW IF EXISTS `v_cidades`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_cidades` AS select `a`.`idcidade` AS `idcidade`,`a`.`descidade` AS `descidade`,`b`.`desestado` AS `desestado` from (`tb_cidades` `a` join `tb_estados` `b` on((`a`.`idestado` = `b`.`idestado`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_estados`
--

/*!50001 DROP VIEW IF EXISTS `v_estados`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_estados` AS select `tb_estados`.`idestado` AS `idestado`,`tb_estados`.`desestado` AS `desestado`,`tb_estados`.`dessigla` AS `dessigla` from `tb_estados` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_lugares`
--

/*!50001 DROP VIEW IF EXISTS `v_lugares`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_lugares` AS select `tb_lugares`.`idlugar` AS `idlugar`,`tb_lugares`.`deslugar` AS `deslugar`,`tb_lugares`.`desendereco` AS `desendereco`,`tb_lugares`.`descep` AS `descep`,`tb_lugares`.`desdescricao` AS `desdescricao`,`tb_lugares`.`deslatitude` AS `deslatitude`,`tb_lugares`.`deslongitude` AS `deslongitude`,`tb_lugares`.`deslogo` AS `deslogo` from `tb_lugares` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_pessoas`
--

/*!50001 DROP VIEW IF EXISTS `v_pessoas`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_pessoas` AS select `a`.`idpessoa` AS `idpessoa`,`a`.`despessoa` AS `despessoa`,`b`.`despessoatipo` AS `despessoatipo` from (`tb_pessoas` `a` join `tb_pessoastipos` `b` on((`a`.`idpessoatipo` = `b`.`idpessoatipo`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_usuarios`
--

/*!50001 DROP VIEW IF EXISTS `v_usuarios`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_usuarios` AS select `a`.`idusuario` AS `idusuario`,`a`.`desusuario` AS `desusuario`,`a`.`dessenha` AS `dessenha`,`b`.`despessoa` AS `despessoa` from (`tb_usuarios` `a` join `tb_pessoas` `b` on((`a`.`idpessoa` = `b`.`idpessoa`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-26 18:24:59
