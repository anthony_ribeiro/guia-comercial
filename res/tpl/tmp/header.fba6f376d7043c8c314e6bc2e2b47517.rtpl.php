<?php if(!class_exists('raintpl')){exit;}?><!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="UTF-8">
		<meta name="author" content="<?php echo $meta_author;?>">
		<meta name="description" content="<?php echo $meta_description;?>">
		<title><?php echo $head_title;?></title>
		<link rel="stylesheet" href="<?php echo $path;?>/lib/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo $path;?>/res/css/estilo.css">
		<link rel="icon" type="image/png" href="<?php echo $path;?>/res/img/icon_diamantina.png">
		<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
		<!-- Important Owl stylesheet -->
		<link rel="stylesheet" href="<?php echo $path;?>/lib/owl-carousel/owl.carousel.css">
		 
		<!-- Default Theme -->
		<link rel="stylesheet" href="<?php echo $path;?>/lib/owl-carousel/owl.theme.css">

		<!-- jquery -->
		<script src="<?php echo $path;?>/lib/jquery/jquery.min.js"></script>

		<script src="<?php echo $path;?>/lib/angular/angular.min.js"></script>
		<script src="<?php echo $path;?>/lib/owl-carousel/owl.carousel.js"></script>
		<script>		
			window.init = [], window.PATH = '<?php echo $path;?>';
		</script>
	</head>
	<body ng-app="guia">

		<header>
			
			<div class="container">

				<div class="row">
					<div id="logo">Caminhos de Diamantina</div>	
				</div>				
				
				<div class="row">
					
					<ul class="nav nav-pills">
						<li role="presentation"><a href="<?php echo $path;?>/">Home</a></li>
						<li role="presentation"><a href="<?php echo $path;?>/">Sobre</a></li>
						<li role="presentation"><a href="<?php echo $path;?>/">Notícias</a></li>
						<li role="presentation"><a href="<?php echo $path;?>/">Contato</a></li>
						<li role="presentation"><input type="text" class="form-control" placeholder="Pesquisar"></li>
					</ul>

				</div>

			</div>

		</header>

		<section>