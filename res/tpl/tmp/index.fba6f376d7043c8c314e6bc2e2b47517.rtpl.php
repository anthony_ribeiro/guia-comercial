<?php if(!class_exists('raintpl')){exit;}?><!-- 	<div ng-controller="listLugares">
		
		<table border="1">
			
			<thead>
				<tr>
					<th>Nome</th>
					<th>CEP</th>
					<th>Descrição</th>
					<th>Logo</th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="lugar in lugares">
					<td>{{lugar.deslugar}}</td>
					<td>{{lugar.descep}}</td>
					<td>{{lugar.desdescricao}}</td>
					<td><img src="<?php echo $path;?>/res/img/{{lugar.deslogo}}" alt="{{lugar.deslugar}}"></td>
				</tr>
			</tbody>

		</table>

	</div> -->

	<div ng-controller="listLugares">

		<div id="banner-logo">			
			<h2>Venha conhecer Diamantina, a cidade de Juscelino Kubitschek e Chica da Silva</h2>
		</div>
		
		<div class="container">						

			<div id="atracoes" class="row">

				<ul>

					<li class="col-md-4">

						<div class="detalhes">
							<img src="<?php echo $path;?>/res/img/icon-restaurante.png" alt="Restaurante">
							<a href="<?php echo $path;?>/categorias/1/lugares">Conheça a rede de restaurantes de Diamantina</a>	
						</div>
						
					</li>

					<li class="col-md-4">

						<div class="detalhes">
						
							<img src="<?php echo $path;?>/res/img/icon-lazer.png" alt="Lazer">
							<a href="<?php echo $path;?>/categorias/5/lugares">Descubra as principais opções de lazer</a>

						</div>

					</li>

					<li class="col-md-4">

						<div class="detalhes">
						
							<img src="<?php echo $path;?>/res/img/icon-turismo.png" alt="Turismo">
							<a href="<?php echo $path;?>/categorias/6/lugares">Saiba quais são os principais pontos turísticos dessa cidade histórica</a>

						</div>

					</li>

				</ul>

			</div>	

			<div class="col-md-6">

				<h3>Categorias</h3>
				
				<ul class="categorias">
					
					<li ng-repeat="categoria in categorias">
						<a href="<?php echo $path;?>/categorias/{{categoria.idcategoria}}/lugares">{{categoria.descategoria}}</a>
					</li>

				</ul>

			</div>

		</div>

	</div>

	<script>
	angular.module("guia", []).controller("listLugares", function($scope, $http){

		$scope.categorias = [];
		$scope.lugares = [];

		$scope.loadCategorias = function(){
			$.store({
				$http:$http,
				cache:false,
				url:PATH+"/lugares/categorias/all",
				success:function(categorias){
					$scope.categorias = categorias;
				},
				failure:function(r){
					System.showError(r);
				}
			});
		};

		$scope.loadLugares = function(){
			$.store({
				$http:$http,
				cache:false,
				url:PATH+"/lugares/all",
				success:function(lugares){
					$scope.lugares = lugares;
				},
				failure:function(r){
					System.showError(r);
				}
			});
		};

		$scope.loadCategorias();
		$scope.loadLugares();

	});
	</script>