<?php if(!class_exists('raintpl')){exit;}?>	<script src="<?php echo $path;?>/res/js/initMap.js"></script>
	<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9Lf1NSrcr-dUAL2uiycmW3XVdyrlHMic&callback=initMap">
    </script>
	<div class="container" ng-controller="listContatos">
		
		<div class="row lugar-detalhes">
			
			<div class="col-md-6">
			
				<h1><?php echo $lugar["deslugar"];?></h1>
				<h4><?php echo $lugar["desendereco"];?>, <?php echo $lugar["desbairro"];?> - <?php echo $lugar["descidade"];?>, <?php echo $lugar["desestado"];?><?php if( !$lugar["descep"] ){ ?><?php }else{ ?>, <?php echo $lugar["descep"];?><?php } ?></h4>
	
				<div id="contato">
					<h3>Entre em contato</h3>
					<a href="tel:{{contato.descontato}}" ng-repeat="contato in contatos"><img src="<?php echo $path;?>/res/img/{{contato.desicone}}" alt="<?php echo $lugar["deslugar"];?>"> {{contato.descontato}}</a>
				</div>
			
			</div>

			<div class="col-md-6" id="map" style="height:250px;" data-lat="<?php echo $lugar["deslatitude"];?>" data-lng="<?php echo $lugar["deslongitude"];?>" data-title="<?php echo $lugar["deslugar"];?>"></div>

		</div>

		<div class="row">

			<div class="col-md-6">
					
				

			</div>

			<div class="col-md-6" id="slides-lugar">
				
				<?php if( !count($fotos > 0) ){ ?>
					<div class="item"><img src="<?php echo $path;?>/res/img/sem-imagem.jpg" alt="Sem imagens"></div>
				<?php }elseif( !count($fotos === 1) ){ ?>
					<div class="item"><img src="<?php echo $path;?>/res/img/<?php echo $fotos["desfoto"];?>" alt="<?php echo $lugar["deslugar"];?>"></div>
				<?php }else{ ?>
					<?php $counter1=-1; if( isset($fotos) && is_array($fotos) && sizeof($fotos) ) foreach( $fotos as $key1 => $value1 ){ $counter1++; ?>
						<div class="item"><img src="<?php echo $path;?>/res/img/<?php echo $value1["desfoto"];?>" alt="<?php echo $lugar["deslugar"];?>"></div>
					<?php } ?>
				<?php } ?>



			</div>

		</div>

	</div>

	<script>
	angular.module("guia", []).controller("listContatos", function($http, $scope){

		$scope.contatos = [];

		var idlugar = "<?php echo $lugar["idlugar"];?>";

		$scope.loadContatos = function(){
			$.store({
				$http:$http,
				cache:false,
				url:PATH+"/lugares/"+idlugar+"/contatos/all",
				success:function(contatos){
					$scope.contatos = contatos;
				},
				failure:function(r){
					System.showError(r);
				}
			})
		};

		$scope.loadFotos = function(){
			$.store({
				$http:$http,
				cache:false,
				url:PATH+"/lugares/"+idlugar+"/fotos/all",
				success:function(fotos){
					$scope.fotos = fotos;
				},
				failure:function(r){
					System.showError(r);
				}
			});
		};

		$scope.loadContatos();
		// $scope.loadFotos();

	});
	</script>